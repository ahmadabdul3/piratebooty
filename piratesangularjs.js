

var pirateBooty = angular.module("pirateBooty", []);
pirateBooty.controller('pirateBootyVM', function($scope, $http) {
    $scope.numOfPirates = "";
    $scope.numOfCoins = "";
    $scope.inputWarning =  {
    	message: "Provide a number between 2 and 999 (if you don't want trouble)",
    	txtColor: "drk-gry-txt"
    };
    $scope.updateWarning = function() {
    	if (isNaN($scope.numOfPirates) || $scope.numOfPirates.indexOf(' ') > -1) {
    		$scope.inputWarning = { 
    			message : "You're a mischievous one aren't you",
    			txtColor : "tomato-txt"
    		};
    	} else if ($scope.numOfPirates == '') {
			$scope.inputWarning = {
				message: "Provide a number between 2 and 999 (if you don't want trouble)",
			    txtColor: "drk-gry-txt"
			};
    	} else if ($scope.numOfPirates < 2 || $scope.numOfPirates > 999) {
    		$scope.inputWarning = {
    			message : "Some people just wanna watch the world burn",
    		    txtColor : "red-txt"
    		};
    	} else {
    		$scope.inputWarning =  {
    			message : "Looks good so far",
    			txtColor : "green-txt"
    		};
    	}
    }
    $scope.getNumOfCoins = function() {
    	$('.modal-background').fadeIn();
 		$('.modal').fadeIn();
    	$http.get("http://pirate.azurewebsites.net/api/Pirate/" + $scope.numOfPirates).
		  success(function(data, status, headers, config) {
		  	$('.modal-background').fadeOut();
 			$('.modal').fadeOut();
 			if (isNaN(data)) 
 				{
 					$scope.numOfCoins = "Come on man.. NUMBERS between 2 and 999 only!";
 					return;
 				}
		  	$scope.numOfCoins = data;
		  }).
		  error(function(data, status, headers, config) {
		  	$('.modal-background').fadeOut();
 			$('.modal').fadeOut();
		    $scope.numOfCoins = "Come on man.. NUMBERS between 2 and 999 only!";
		  });
    }
    
});



